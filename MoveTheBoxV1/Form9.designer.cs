namespace MoveTheBoxV1
{
    partial class Form9
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(Form9));
            this.button_backgroundOk = new System.Windows.Forms.Button();
            this.richTextBox_rules = new System.Windows.Forms.RichTextBox();
            this.SuspendLayout();
            // 
            // button_backgroundOk
            // 
            this.button_backgroundOk.BackColor = System.Drawing.Color.Wheat;
            this.button_backgroundOk.Location = new System.Drawing.Point(357, 210);
            this.button_backgroundOk.Name = "button_backgroundOk";
            this.button_backgroundOk.Size = new System.Drawing.Size(83, 31);
            this.button_backgroundOk.TabIndex = 2;
            this.button_backgroundOk.Text = "OK";
            this.button_backgroundOk.UseVisualStyleBackColor = false;
            this.button_backgroundOk.Click += new System.EventHandler(this.button_backgroundOk_Click);
            // 
            // richTextBox_rules
            // 
            this.richTextBox_rules.BackColor = System.Drawing.Color.Wheat;
            this.richTextBox_rules.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.richTextBox_rules.Location = new System.Drawing.Point(12, 12);
            this.richTextBox_rules.Name = "richTextBox_rules";
            this.richTextBox_rules.ReadOnly = true;
            this.richTextBox_rules.ScrollBars = System.Windows.Forms.RichTextBoxScrollBars.ForcedVertical;
            this.richTextBox_rules.Size = new System.Drawing.Size(428, 192);
            this.richTextBox_rules.TabIndex = 3;
            this.richTextBox_rules.Text = resources.GetString("richTextBox_rules.Text");
            // 
            // Form9
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.Wheat;
            this.ClientSize = new System.Drawing.Size(444, 244);
            this.Controls.Add(this.richTextBox_rules);
            this.Controls.Add(this.button_backgroundOk);
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.MaximizeBox = false;
            this.MaximumSize = new System.Drawing.Size(460, 280);
            this.MinimumSize = new System.Drawing.Size(460, 280);
            this.Name = "Form9";
            this.SizeGripStyle = System.Windows.Forms.SizeGripStyle.Hide;
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Rules";
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Button button_backgroundOk;
        private System.Windows.Forms.RichTextBox richTextBox_rules;
    }
}