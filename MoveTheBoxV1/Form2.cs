using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.IO;
using System.Windows.Forms;
using System.Runtime.Serialization;
using System.Runtime.Serialization.Formatters;
using System.Runtime.Serialization.Formatters.Binary;
using PositionLibrary;
using LevelLibrary;


namespace MoveTheBoxV1
{
    public partial class Form2 : Form
    {
        public Form1 form1;
        public string _playerName;
        public Panel[,] _panelMatrix;
        private string[,] _objectMatrix;
        public Color[] _defaultColors;
        public Color _groundColor;
        public Color _wallColor;
        public Color _boxColor;
        public Color _boxOnGoalColor;
        public Color _goalColor;
        private Color _groundColorTextureMode;
        private Position _currentPos;
        private Bitmap _left1;
        private Bitmap _left2;
        private Bitmap _right1;
        private Bitmap _right2;
        private Bitmap _up1;
        private Bitmap _up2;
        private Bitmap _down1;
        private Bitmap _down2;
        private Bitmap _currentStand;
        private Bitmap _brick;
        private Bitmap _box;
        private Bitmap _goal;
        private Bitmap _boxOnGoal;
        private bool _released;
        private List<Position> _goalFields;
        private List<Position> _boxFieldsStart;
        private List<Position> _wallFields;
        public int _currentLevel;
        private int _maxLevel;
        private bool _newGame;
        private List<Record> _records;
        public Record _profile;
        public DateTime _startTime;
        private bool _loadedSuccessfully;
        public bool _textureMode;
        private bool _done;
        private Levels _levelContainer;
        public int _moves;
        public bool _infoBox;
        public TimeSpan _timeDifference;

        public Form2()
        {
            InitializeComponent();
            loadImages();
            loadLevels();
            _currentStand = _down1;
            _defaultColors = new Color[5];
            _defaultColors[0] = Color.Wheat;
            _defaultColors[1] = Color.SaddleBrown;
            _defaultColors[2] = Color.Peru;
            _defaultColors[3] = Color.YellowGreen;
            _defaultColors[4] = Color.Orange;
            _groundColor = _defaultColors[0];
            _wallColor = _defaultColors[1];
            _boxColor = _defaultColors[2];
            _goalColor = _defaultColors[3];
            _boxOnGoalColor = _defaultColors[4];
            _groundColorTextureMode = Color.Wheat;
            _currentPos = new Position();
            _released = true;
            _goalFields = new List<Position>();
            _boxFieldsStart = new List<Position>();
            _wallFields = new List<Position>();
            _currentLevel = 0; //0 is actually loading _currentLevel = 1
            _maxLevel = 50;
            _newGame = false;
            _records = new List<Record>();
            _profile = new Record();
            _startTime = DateTime.MaxValue;
            _loadedSuccessfully = false;
            _textureMode = true;
            _done = false;
            _moves = 0;
            _infoBox = false;
            _timeDifference = TimeSpan.MaxValue;

            //initialize panel matrix
            _panelMatrix = new Panel[25, 25] {{panel1, panel2, panel3, panel4, panel5, panel6, panel7, panel8, panel9, panel10, panel11, panel12, panel13, panel14, panel15, panel16, panel17, panel18, panel19, panel20, panel21, panel22, panel23, panel24, panel25},
                                              {panel41, panel42, panel43, panel44, panel45, panel46, panel47, panel48, panel49, panel50, panel51, panel52, panel53, panel54, panel55, panel56, panel57, panel58, panel59, panel60, panel61, panel62, panel63, panel64, panel65},
                                              {panel81, panel82, panel83, panel84, panel85, panel86, panel87, panel88, panel89, panel90, panel91, panel92, panel93, panel94, panel95, panel96, panel97, panel98, panel99, panel100, panel101, panel102, panel103, panel104, panel105},
                                              {panel121, panel122, panel123, panel124, panel125, panel126, panel127, panel128, panel129, panel130, panel131, panel132, panel133, panel134, panel135, panel136, panel137, panel138, panel139, panel140, panel141, panel142, panel143, panel144, panel145},
                                              {panel161, panel162, panel163, panel164, panel165, panel166, panel167, panel168, panel169, panel170, panel171, panel172, panel173, panel174, panel175, panel176, panel177, panel178, panel179, panel180, panel181, panel182, panel183, panel184, panel185},
                                              {panel201, panel202, panel203, panel204, panel205, panel206, panel207, panel208, panel209, panel210, panel211, panel212, panel213, panel214, panel215, panel216, panel217, panel218, panel219, panel220, panel221, panel222, panel223, panel224, panel225},
                                              {panel241, panel242, panel243, panel244, panel245, panel246, panel247, panel248, panel249, panel250, panel251, panel252, panel253, panel254, panel255, panel256, panel257, panel258, panel259, panel260, panel261, panel262, panel263, panel264, panel265},
                                              {panel281, panel282, panel283, panel284, panel285, panel286, panel287, panel288, panel289, panel290, panel291, panel292, panel293, panel294, panel295, panel296, panel297, panel298, panel299, panel300, panel301, panel302, panel303, panel304, panel305},
                                              {panel321, panel322, panel323, panel324, panel325, panel326, panel327, panel328, panel329, panel330, panel331, panel332, panel333, panel334, panel335, panel336, panel337, panel338, panel339, panel340, panel341, panel342, panel343, panel344, panel345},
                                              {panel361, panel362, panel363, panel364, panel365, panel366, panel367, panel368, panel369, panel370, panel371, panel372, panel373, panel374, panel375, panel376, panel377, panel378, panel379, panel380, panel381, panel382, panel383, panel384, panel385},
                                              {panel401, panel402, panel403, panel404, panel405, panel406, panel407, panel408, panel409, panel410, panel411, panel412, panel413, panel414, panel415, panel416, panel417, panel418, panel419, panel420, panel421, panel422, panel423, panel424, panel425},
                                              {panel441, panel442, panel443, panel444, panel445, panel446, panel447, panel448, panel449, panel450, panel451, panel452, panel453, panel454, panel455, panel456, panel457, panel458, panel459, panel460, panel461, panel462, panel463, panel464, panel465},
                                              {panel481, panel482, panel483, panel484, panel485, panel486, panel487, panel488, panel489, panel490, panel491, panel492, panel493, panel494, panel495, panel496, panel497, panel498, panel499, panel500, panel501, panel502, panel503, panel504, panel505},
                                              {panel521, panel522, panel523, panel524, panel525, panel526, panel527, panel528, panel529, panel530, panel531, panel532, panel533, panel534, panel535, panel536, panel537, panel538, panel539, panel540, panel541, panel542, panel543, panel544, panel545},
                                              {panel561, panel562, panel563, panel564, panel565, panel566, panel567, panel568, panel569, panel570, panel571, panel572, panel573, panel574, panel575, panel576, panel577, panel578, panel579, panel580, panel581, panel582, panel583, panel584, panel585},
                                              {panel601, panel602, panel603, panel604, panel605, panel606, panel607, panel608, panel609, panel610, panel611, panel612, panel613, panel614, panel615, panel616, panel617, panel618, panel619, panel620, panel621, panel622, panel623, panel624, panel625},
                                              {panel641, panel642, panel643, panel644, panel645, panel646, panel647, panel648, panel649, panel650, panel651, panel652, panel653, panel654, panel655, panel656, panel657, panel658, panel659, panel660, panel661, panel662, panel663, panel664, panel665},
                                              {panel681, panel682, panel683, panel684, panel685, panel686, panel687, panel688, panel689, panel690, panel691, panel692, panel693, panel694, panel695, panel696, panel697, panel698, panel699, panel700, panel701, panel702, panel703, panel704, panel705},
                                              {panel721, panel722, panel723, panel724, panel725, panel726, panel727, panel728, panel729, panel730, panel731, panel732, panel733, panel734, panel735, panel736, panel737, panel738, panel739, panel740, panel741, panel742, panel743, panel744, panel745},
                                              {panel761, panel762, panel763, panel764, panel765, panel766, panel767, panel768, panel769, panel770, panel771, panel772, panel773, panel774, panel775, panel776, panel777, panel778, panel779, panel780, panel781, panel782, panel783, panel784, panel785},
                                              {panel801, panel802, panel803, panel804, panel805, panel806, panel807, panel808, panel809, panel810, panel811, panel812, panel813, panel814, panel815, panel816, panel817, panel818, panel819, panel820, panel821, panel822, panel823, panel824, panel825},
                                              {panel841, panel842, panel843, panel844, panel845, panel846, panel847, panel848, panel849, panel850, panel851, panel852, panel853, panel854, panel855, panel856, panel857, panel858, panel859, panel860, panel861, panel862, panel863, panel864, panel865},
                                              {panel881, panel882, panel883, panel884, panel885, panel886, panel887, panel888, panel889, panel890, panel891, panel892, panel893, panel894, panel895, panel896, panel897, panel898, panel899, panel900, panel901, panel902, panel903, panel904, panel905},
                                              {panel921, panel922, panel923, panel924, panel925, panel926, panel927, panel928, panel929, panel930, panel931, panel932, panel933, panel934, panel935, panel936, panel937, panel938, panel939, panel940, panel941, panel942, panel943, panel944, panel945},
                                              {panel961, panel962, panel963, panel964, panel965, panel966, panel967, panel968, panel969, panel970, panel971, panel972, panel973, panel974, panel975, panel976, panel977, panel978, panel979, panel980, panel981, panel982, panel983, panel984, panel985}};
                                              
          
            //initialize object matrix
            _objectMatrix = new string[25, 25];

            //load level 1
            changeLevel();
            _infoBox = true;
        }

        private void Form2_Load(object sender, EventArgs e)
        {
            _playerName = form1._playerName;
            label_name.Text = "Name: " + _playerName;
            label_level.Text = "Level " + _currentLevel.ToString().PadLeft(2, '0');
            loadRecordsFromFile();
            loadProfile();
        }

        private void Form2_FormClosed(object sender, FormClosedEventArgs e)
        {
            if (!_newGame)
            {
                form1.Close();
            }

            saveRecordsToFile();
        }

        private void loadImages()
        {
            //load character
            _left1 = (Bitmap)Bitmap.FromFile("Textures/left1.bmp");
            _left1.MakeTransparent(Color.Wheat);
            _left2 = (Bitmap)Bitmap.FromFile("Textures/left2.bmp");
            _left2.MakeTransparent(Color.Wheat);
            _right1 = (Bitmap)Bitmap.FromFile("Textures/right1.bmp");
            _right1.MakeTransparent(Color.Wheat);
            _right2 = (Bitmap)Bitmap.FromFile("Textures/right2.bmp");
            _right2.MakeTransparent(Color.Wheat);
            _up1 = (Bitmap)Bitmap.FromFile("Textures/up1.bmp");
            _up1.MakeTransparent(Color.Wheat);
            _up2 = (Bitmap)Bitmap.FromFile("Textures/up2.bmp");
            _up2.MakeTransparent(Color.Wheat);
            _down1 = (Bitmap)Bitmap.FromFile("Textures/down1.bmp");
            _down1.MakeTransparent(Color.Wheat);
            _down2 = (Bitmap)Bitmap.FromFile("Textures/down2.bmp");
            _down2.MakeTransparent(Color.Wheat);

            //load textures
            _brick = (Bitmap)Bitmap.FromFile("Textures/brick.bmp");
            _box = (Bitmap)Bitmap.FromFile("Textures/box.bmp");
            _boxOnGoal = (Bitmap)Bitmap.FromFile("Textures/boxOnGoal.bmp");
            _goal = (Bitmap)Bitmap.FromFile("Textures/goal.bmp");
            _goal.MakeTransparent(Color.White);
        }

        private void Form2_KeyDown(object sender, KeyEventArgs e)
        {
            if (!_done)
            {
                if (_released)
                {
                    int row = _currentPos.getRow();
                    int col = _currentPos.getCol();
                    
                    if (e.KeyCode == Keys.Left)
                    {
                        if (_currentStand == _left1)
                        {
                            _currentStand = _left2;
                        }
                        else
                        {
                            _currentStand = _left1;
                        }
                        if (_panelMatrix[row, col].BackgroundImage != null)
                        {
                            if (_textureMode && _goalFields.Contains(new Position(row, col)))
                            {
                                _panelMatrix[row, col].BackColor = _groundColorTextureMode;
                                _panelMatrix[row, col].BackgroundImage = _goal;
                            }
                            else
                            {
                                _panelMatrix[row, col].BackgroundImage = null;
                            }
                        }
                        if (evalutateCollision("left", _currentPos))
                        {
                            col--;
                            _moves++;
                        }
                        _panelMatrix[row, col].BackgroundImage = _currentStand;
                    }
                    else if (e.KeyCode == Keys.Right)
                    {
                        if (_currentStand == _right1)
                        {
                            _currentStand = _right2;
                        }
                        else
                        {
                            _currentStand = _right1;
                        }
                        if (_panelMatrix[row, col].BackgroundImage != null)
                        {
                            if (_textureMode && _goalFields.Contains(new Position(row, col)))
                            {
                                _panelMatrix[row, col].BackColor = _groundColorTextureMode;
                                _panelMatrix[row, col].BackgroundImage = _goal;
                            }
                            else
                            {
                                _panelMatrix[row, col].BackgroundImage = null;
                            }
                        }
                        if (evalutateCollision("right", _currentPos))
                        {
                            col++;
                            _moves++;
                        }
                        _panelMatrix[row, col].BackgroundImage = _currentStand;
                    }
                    else if (e.KeyCode == Keys.Up)
                    {
                        if (_currentStand == _up1)
                        {
                            _currentStand = _up2;
                        }
                        else
                        {
                            _currentStand = _up1;
                        }
                        if (_panelMatrix[row, col].BackgroundImage != null)
                        {
                            if (_textureMode && _goalFields.Contains(new Position(row, col)))
                            {
                                _panelMatrix[row, col].BackColor = _groundColorTextureMode;
                                _panelMatrix[row, col].BackgroundImage = _goal;
                            }
                            else
                            {
                                _panelMatrix[row, col].BackgroundImage = null;
                            }
                        }
                        if (evalutateCollision("up", _currentPos))
                        {
                            row--;
                            _moves++;
                        }
                        _panelMatrix[row, col].BackgroundImage = _currentStand;
                    }
                    else if (e.KeyCode == Keys.Down)
                    {
                        if (_currentStand == _down1)
                        {
                            _currentStand = _down2;
                        }
                        else
                        {
                            _currentStand = _down1;
                        }
                        if (_panelMatrix[row, col].BackgroundImage != null)
                        {
                            if (_textureMode && _goalFields.Contains(new Position(row, col)))
                            {
                                _panelMatrix[row, col].BackColor = _groundColorTextureMode;
                                _panelMatrix[row, col].BackgroundImage = _goal;
                            }
                            else
                            {
                                _panelMatrix[row, col].BackgroundImage = null;
                            }
                        }
                        if (evalutateCollision("down", _currentPos))
                        {
                            row++;
                            _moves++;
                        }
                        _panelMatrix[row, col].BackgroundImage = _currentStand;
                    }

                    _currentPos.setRow(row);
                    _currentPos.setCol(col);

                    _released = false;

                    //after each time the guy moved, check if all boxes are in the goal fields
                    if (allGoalFieldsFilled())
                    {
                        _timeDifference = getTimeDifference(_startTime, DateTime.Now);
                        _profile.update(_currentLevel, _timeDifference, _moves);
                        updateLevelMenu();
                        //load next level and show time
                        Form6 showTime = new Form6();
                        showTime.form2 = this;
                        showTime.ShowDialog();
                        changeLevel();
                    }
                }
            }
        }

        private void Form2_KeyUp(object sender, KeyEventArgs e)
        {
            _released = true;
        }

        private bool evalutateCollision(string direction, Position position)
        {
            bool move = true;
            int row = position.getRow();
            int col = position.getCol();

            if (direction == "left")
            {
                if (_objectMatrix[row, col - 1] == "wall")
                {
                    move = false;
                }
                else if (_objectMatrix[row, col - 1] == "box")
                {
                    if (_objectMatrix[row, col - 2] == "wall" || _objectMatrix[row, col - 2] == "box")
                    {
                        move = false;
                    }
                    else
                    {
                        moveBox(row, col - 1, "left");
                    }
                }
            }
            else if (direction == "right")
            {
                if (_objectMatrix[row, col + 1] == "wall")
                {
                    move = false;
                }
                else if (_objectMatrix[row, col + 1] == "box")
                {
                    if (_objectMatrix[row, col + 2] == "wall" || _objectMatrix[row, col + 2] == "box")
                    {
                        move = false;
                    }
                    else
                    {
                        moveBox(row, col + 1, "right");
                    }
                }
            }
            else if (direction == "up")
            {
                if (_objectMatrix[row - 1, col] == "wall")
                {
                    move = false;
                }
                else if (_objectMatrix[row - 1, col] == "box")
                {
                    if (_objectMatrix[row - 2, col] == "wall" || _objectMatrix[row - 2, col] == "box")
                    {
                        move = false;
                    }
                    else
                    {
                        moveBox(row - 1, col, "up");
                    }
                }
            }
            else if (direction == "down")
            {
                if (_objectMatrix[row + 1, col] == "wall")
                {
                    move = false;
                }
                else if (_objectMatrix[row + 1, col] == "box")
                {
                    if (_objectMatrix[row + 2, col] == "wall" || _objectMatrix[row + 2, col] == "box")
                    {
                        move = false;
                    }
                    else
                    {
                        moveBox(row + 1, col, "down");
                    }
                }
            }

            return move;
        }

        private void moveBox(int row, int col, string direction)
        {
            //row, col are position of the box

            //delete box at the current position
            _objectMatrix[row, col] = "";
            if (_goalFields.Contains(new Position(row, col)))
            {
                if (_textureMode)
                {
                    _panelMatrix[row, col].BackColor = _groundColorTextureMode;
                    _panelMatrix[row, col].BackgroundImage = _goal;
                }
                else
                {
                    _panelMatrix[row, col].BackColor = _goalColor;
                }
                _objectMatrix[row, col] = "goal";
            }
            else
            {
                if (_textureMode)
                {
                    _panelMatrix[row, col].BackColor = _groundColorTextureMode;
                    _panelMatrix[row, col].BackgroundImage = null;
                }
                else
                {
                    _panelMatrix[row, col].BackColor = _groundColor;
                }
            }

            //draw box at new position
            if (direction == "left")
            {
                if (_textureMode)
                {
                    if (_goalFields.Contains(new Position(row, col - 1)))
                    {
                        _panelMatrix[row, col - 1].BackgroundImage = _boxOnGoal;
                    }
                    else
                    {
                        _panelMatrix[row, col - 1].BackgroundImage = _box;
                    }
                }
                else
                {
                    if (_goalFields.Contains(new Position(row, col - 1)))
                    {
                        _panelMatrix[row, col - 1].BackColor = _boxOnGoalColor;
                    }
                    else
                    {
                        _panelMatrix[row, col - 1].BackColor = _boxColor;
                    }
                }
                _objectMatrix[row, col - 1] = "box";
            }
            else if (direction == "right")
            {
                if (_textureMode)
                {
                    if (_goalFields.Contains(new Position(row, col + 1)))
                    {
                        _panelMatrix[row, col + 1].BackgroundImage = _boxOnGoal;
                    }
                    else
                    {
                        _panelMatrix[row, col + 1].BackgroundImage = _box;
                    }
                }
                else
                {
                    if (_goalFields.Contains(new Position(row, col + 1)))
                    {
                        _panelMatrix[row, col + 1].BackColor = _boxOnGoalColor;
                    }
                    else
                    {
                        _panelMatrix[row, col + 1].BackColor = _boxColor;
                    }
                }
                _objectMatrix[row, col + 1] = "box";                 
            }
            else if (direction == "up")
            {
                if (_textureMode)
                {
                    if (_goalFields.Contains(new Position(row - 1, col)))
                    {
                        _panelMatrix[row - 1, col].BackgroundImage = _boxOnGoal;
                    }
                    else
                    {
                        _panelMatrix[row - 1, col].BackgroundImage = _box;
                    }
                }
                else
                {
                    if (_goalFields.Contains(new Position(row - 1, col)))
                    {
                        _panelMatrix[row - 1, col].BackColor = _boxOnGoalColor;
                    }
                    else
                    {
                        _panelMatrix[row - 1, col].BackColor = _boxColor;
                    }
                }
                _objectMatrix[row - 1, col] = "box";                
            }
            else if (direction == "down")
            {
                if (_textureMode)
                {
                    if (_goalFields.Contains(new Position(row + 1, col)))
                    {
                        _panelMatrix[row + 1, col].BackgroundImage = _boxOnGoal;
                    }
                    else
                    {
                        _panelMatrix[row + 1, col].BackgroundImage = _box;
                    }
                }
                else
                {
                    if (_goalFields.Contains(new Position(row + 1, col)))
                    {
                        _panelMatrix[row + 1, col].BackColor = _boxOnGoalColor;
                    }
                    else
                    {
                        _panelMatrix[row + 1, col].BackColor = _boxColor;
                    }
                }
                _objectMatrix[row + 1, col] = "box";             
            }
        }

        private bool allGoalFieldsFilled()
        {
            bool abort = false;
            int count = 0;

            while (!abort && count < _goalFields.Count)
            {
                if (_objectMatrix[_goalFields[count].getRow(), _goalFields[count].getCol()] != "box")
                {
                    abort = true;
                }

                count++;
            }

            return !abort;
        }

        private void drawLevel()
        {
            for (int row = 0; row < 25; row++)
            {
                for (int col = 0; col < 25; col++)
                {
                    if (_wallFields.Contains(new Position(row, col)))
                    {
                        if (_textureMode)
                        {
                            _panelMatrix[row, col].BackColor = _groundColorTextureMode;
                            _panelMatrix[row, col].BackgroundImage = _brick;
                        }
                        else
                        {
                            _panelMatrix[row, col].BackColor = _wallColor;
                        }
                        _objectMatrix[row, col] = "wall";
                    }
                    else if (_boxFieldsStart.Contains(new Position(row, col)) &&
                        _goalFields.Contains(new Position(row, col)))
                    {
                        if (_textureMode)
                        {
                            _panelMatrix[row, col].BackColor = _groundColorTextureMode;
                            _panelMatrix[row, col].BackgroundImage = _boxOnGoal;
                        }
                        else
                        {
                            _panelMatrix[row, col].BackColor = _boxOnGoalColor;
                        }
                        _objectMatrix[row, col] = "box";
                    }
                    else if (_boxFieldsStart.Contains(new Position(row, col)))
                    {
                        if (_textureMode)
                        {
                            _panelMatrix[row, col].BackColor = _groundColorTextureMode;
                            _panelMatrix[row, col].BackgroundImage = _box;
                        }
                        else
                        {
                            _panelMatrix[row, col].BackColor = _boxColor;
                        }
                        _objectMatrix[row, col] = "box";
                    }
                    else if (_goalFields.Contains(new Position(row, col)))
                    {
                        if (_textureMode)
                        {
                            _panelMatrix[row, col].BackColor = _groundColorTextureMode;
                            _panelMatrix[row, col].BackgroundImage = _goal;
                        }
                        else
                        {
                            _panelMatrix[row, col].BackColor = _goalColor;
                        }
                        _objectMatrix[row, col] = "goal";
                    }
                    else
                    {
                        if (_textureMode)
                        {
                            _panelMatrix[row, col].BackColor = _groundColorTextureMode;
                        }
                        else
                        {
                            _panelMatrix[row, col].BackColor = _groundColor;
                        }
                        _objectMatrix[row, col] = "";
                    }
                }
            }
        }

        private void showInfoBox()
        {
            Form5 info = new Form5();
            info.form2 = this;
            info.ShowDialog();
        }

        private void clearLevel()
        {
            for (int row = 0; row < 25; row++)
            {
                for (int col = 0; col < 25; col++)
                {
                    _panelMatrix[row, col].BackgroundImage = null;
                    _panelMatrix[row, col].BackColor = Color.White;
                    _objectMatrix[row, col] = "";
                }
            }
            _goalFields.Clear();
            _boxFieldsStart.Clear();
            _wallFields.Clear();
            _moves = 0;
            _released = true;
        }

        private void exitToolStripMenuItem_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void newToolStripMenuItem_Click(object sender, EventArgs e)
        {
            Form8 messageBox = new Form8();
            messageBox.ShowDialog();

            if (!messageBox._cancel)
            {
                if (messageBox._sameName)
                {
                    _currentLevel = 0;
                    label_level.Text = "Level " + _currentLevel.ToString().PadLeft(2, '0');
                    clearLevel();
                    changeLevel();
                    saveToolStripMenuItem.Enabled = true;
                }
                else
                {
                    _newGame = true;
                    form1.Location = form1._centerScreenLocation;
                    form1.textBox_name.Text = "";
                    this.Close();
                    form1.Show();
                    form1.Focus();
                }              
            }
        }

        private void restartLevelToolStripMenuItem_Click(object sender, EventArgs e)
        {
            int value = 1;
            if (_done)
            {
                value = 2;
                _done = false;
            }
            _currentLevel -= value;
            changeLevel();
        }

        private void colorsToolStripMenuItem_Click(object sender, EventArgs e)
        {
            Form3 colors = new Form3();
            colors.form2 = this;
            colors.ShowDialog();
        }

        private void loadProfile()
        {
            Record searchRecord = new Record(_playerName);
            if (_records.Contains(searchRecord))
            {
                int index = _records.IndexOf(searchRecord);
                _profile = _records[index];
            }
            else //create record
            {
                _profile = new Record(_playerName);
                _records.Add(_profile);
            }
            updateLevelMenu();
        }

        public TimeSpan getTimeDifference(DateTime startTime, DateTime endTime)
        {
            return endTime.Subtract(startTime);
        }

        private void loadRecordsFromFile()
        {
            try
            {
                Stream stream = File.Open("records.log", FileMode.Open);
                BinaryFormatter formatter = new BinaryFormatter();
                _records = (List<Record>)formatter.Deserialize(stream);
                stream.Close();
            }
            catch
            {
                try
                {
                    Stream stream = File.Create("records.log");
                    BinaryFormatter formatter = new BinaryFormatter();
                    formatter.Serialize(stream, _records);
                    stream.Close();
                }
                catch { }
            }
        }

        private void saveRecordsToFile()
        {
            try
            {
                Stream stream = File.Create("records.log");
                BinaryFormatter formatter = new BinaryFormatter();
                formatter.Serialize(stream, _records);
                stream.Close();
            }
            catch { }
        }

        private void saveToolStripMenuItem_Click(object sender, EventArgs e)
        {
            //settings for the save box
            saveFileDialog_game.Filter = "Move The Box File (.mtb)|*.mtb";
            saveFileDialog_game.CheckPathExists = true;
            saveFileDialog_game.AddExtension = true;
            saveFileDialog_game.CreatePrompt = false;
            saveFileDialog_game.DefaultExt = "mtb";
            saveFileDialog_game.OverwritePrompt = true;
            saveFileDialog_game.RestoreDirectory = false;
            saveFileDialog_game.ValidateNames = true;
            saveFileDialog_game.ShowDialog();
        }

        private void saveFileDialog_game_FileOk(object sender, CancelEventArgs e)
        {
            string fileName = saveFileDialog_game.FileName;
            string extension = fileName.Substring(fileName.Length - 4);

            if (extension != ".mtb")
            {
                fileName += ".mtb";
            }

            if (fileName != "")
            {
                try
                {
                    FileInformation info = new FileInformation();

                    info._playerName = _playerName;
                    info._level = _currentLevel;
                    //order: ground, wall, box, goal
                    info._colors[0] = _groundColor.ToArgb();
                    info._colors[1] = _wallColor.ToArgb();
                    info._colors[2] = _boxColor.ToArgb();
                    info._colors[3] = _goalColor.ToArgb();
                    info._colors[4] = _boxOnGoalColor.ToArgb();

                    Stream stream = File.Create(fileName);
                    BinaryFormatter formatter = new BinaryFormatter();
                    formatter.Serialize(stream, info);
                    stream.Close();
                }
                catch
                {
                    MessageBox.Show("Error occured while saving the file!", "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
                }
            }
        }

        private void loadToolStripMenuItem_Click(object sender, EventArgs e)
        {
            //settings for the open box
            openFileDialog_game.Filter = "Move The Box File (.mtb)|*.mtb";
            openFileDialog_game.CheckPathExists = true;
            openFileDialog_game.AddExtension = true;
            openFileDialog_game.DefaultExt = "mtb";
            openFileDialog_game.RestoreDirectory = false;
            openFileDialog_game.ValidateNames = true;
            openFileDialog_game.ShowDialog();

            //after window is closed show infobox
            if (_loadedSuccessfully)
            {
                loadProfile();
                label_name.Text = "Name: " + _playerName;
                _currentLevel--;
                changeLevel();
                _loadedSuccessfully = false;
                _done = false;
                saveToolStripMenuItem.Enabled = true;
            }
        }

        private void openFileDialog_game_FileOk(object sender, CancelEventArgs e)
        {
            string fileName = openFileDialog_game.FileName;

            if (fileName != "")
            {
                try
                {
                    Stream stream = File.Open(fileName, FileMode.Open);
                    BinaryFormatter formatter = new BinaryFormatter();
                    FileInformation info = (FileInformation)formatter.Deserialize(stream);
                    stream.Close();

                    _playerName = info._playerName;
                    _currentLevel = info._level;
                    //order: ground, wall, box, goal
                    _groundColor = Color.FromArgb(info._colors[0]);
                    _wallColor = Color.FromArgb(info._colors[1]);
                    _boxColor = Color.FromArgb(info._colors[2]);
                    _goalColor = Color.FromArgb(info._colors[3]);
                    _boxOnGoalColor = Color.FromArgb(info._colors[4]);

                    _loadedSuccessfully = true;
                }
                catch
                {
                    MessageBox.Show("Error occured while loading the file!", "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
                }
            }
        }

        private void recordToolStripMenuItem_Click(object sender, EventArgs e)
        {
            Form4 record = new Form4();
            record.form2 = this;
            record.ShowDialog();
        }

        private void Form2_Shown(object sender, EventArgs e)
        {
            showInfoBox();
        }

        private void textureModeToolStripMenuItem_CheckedChanged(object sender, EventArgs e)
        {
            if (textureModeToolStripMenuItem.Checked)
            {
                _textureMode = true;
                colorsToolStripMenuItem.Enabled = false;
            }
            else
            {
                _textureMode = false;
                colorsToolStripMenuItem.Enabled = true;
            }
            redrawPanelMatrix();
        }

        private void redrawPanelMatrix()
        {
            for (int row = 0; row < 25; row++)
            {
                for (int col = 0; col < 25; col++)
                {
                    if (_textureMode)
                    {
                        if (_currentPos.getRow() == row && _currentPos.getCol() == col)
                        {
                            _panelMatrix[row, col].BackColor = _groundColorTextureMode;
                            _panelMatrix[row, col].BackgroundImage = _currentStand;
                        }
                        else if (_objectMatrix[row, col] == "wall")
                        {
                            _panelMatrix[row, col].BackColor = _groundColorTextureMode;
                            _panelMatrix[row, col].BackgroundImage = _brick;
                        }
                        else if (_objectMatrix[row, col] == "box")
                        {
                            if (_goalFields.Contains(new Position(row, col)))
                            {
                                _panelMatrix[row, col].BackColor = _boxOnGoalColor;
                                _panelMatrix[row, col].BackgroundImage = _boxOnGoal;
                            }
                            else
                            {
                                _panelMatrix[row, col].BackColor = _groundColorTextureMode;
                                _panelMatrix[row, col].BackgroundImage = _box;
                            }
                        }
                        else if (_objectMatrix[row, col] == "goal")
                        {
                            _panelMatrix[row, col].BackColor = _groundColorTextureMode;
                            _panelMatrix[row, col].BackgroundImage = _goal;
                        }
                        else if (_objectMatrix[row, col] == "")
                        {
                            _panelMatrix[row, col].BackColor = _groundColorTextureMode;
                            _panelMatrix[row, col].BackgroundImage = null;
                        }
                    }
                    else
                    {
                        _panelMatrix[row, col].BackgroundImage = null;
                        if (_currentPos.getRow() == row && _currentPos.getCol() == col)
                        {
                            if (_goalFields.Contains(new Position(row, col)))
                            {
                                _panelMatrix[row, col].BackColor = _goalColor;
                            }
                            else
                            {
                                _panelMatrix[row, col].BackColor = _groundColor;
                            }
                            _panelMatrix[row, col].BackgroundImage = _currentStand;
                        }
                        else if (_objectMatrix[row, col] == "wall")
                        {
                            _panelMatrix[row, col].BackColor = _wallColor;
                        }
                        else if (_objectMatrix[row, col] == "box")
                        {
                            if (_goalFields.Contains(new Position(row, col)))
                            {
                                _panelMatrix[row, col].BackColor = _boxOnGoalColor;
                            }
                            else
                            {
                                _panelMatrix[row, col].BackColor = _boxColor;
                            }
                        }
                        else if (_objectMatrix[row, col] == "goal")
                        {
                            _panelMatrix[row, col].BackColor = _goalColor;
                        }
                        else if (_objectMatrix[row, col] == "")
                        {
                            _panelMatrix[row, col].BackColor = _groundColor;
                        }
                    }
                }
            }
        }

        private void changeLevelHelper()
        {
            clearLevel();
            label_level.Text = "Level " + _currentLevel.ToString().PadLeft(2, '0');

            Level level = _levelContainer.getLevel(_currentLevel);
            configureLevel(level);

            if (_infoBox)
            {
                showInfoBox();
            }
        }

        public void changeLevel()
        {  
            _currentLevel++;
            if (_currentLevel <= _maxLevel)
            {
                changeLevelHelper();
            }
            else
            {
                //show messagebox that you finished the game and don't take any more inputs
                _done = true;
                saveToolStripMenuItem.Enabled = false;
                Form10 finished = new Form10();
                finished.ShowDialog();
            }           
        }

        private void loadLevels()
        {
            try
            {
                Stream stream = File.Open("levels.lvl", FileMode.Open);
                BinaryFormatter formatter = new BinaryFormatter();
                _levelContainer = (Levels)formatter.Deserialize(stream);
                stream.Close();
            }
            catch { }
        }

        private void configureLevel(Level level)
        {
            //starting positon
            _currentPos.setRow(level.getStartPosition().getRow());
            _currentPos.setCol(level.getStartPosition().getCol());
            _currentStand = _down1;

            //goal fields
            foreach (Position pos in level.getGoalFields())
            {
                _goalFields.Add(pos);
            }

            //add boxes
            foreach (Position pos in level.getBoxFields())
            {
                _boxFieldsStart.Add(pos);
            }

            //add walls to the level
            foreach (Position pos in level.getWallFields())
            {
                _wallFields.Add(pos);
            }

            drawLevel();
            _panelMatrix[_currentPos.getRow(), _currentPos.getCol()].BackgroundImage = _currentStand;
        }

        private void rulesToolStripMenuItem_Click(object sender, EventArgs e)
        {
            Form9 rules = new Form9();
            rules.ShowDialog();
        }

        private void aboutToolStripMenuItem1_Click(object sender, EventArgs e)
        {
            Form11 about = new Form11();
            about.ShowDialog();
        }

        private void updateLevelMenu()
        {
            if (_profile.getLevelCount() + 1 >= 2)
            {
                if (_profile.getLevelCount() + 1 >= 2)
                {
                    level02ToolStripMenuItem.Enabled = true;
                }
                else
                {
                    level02ToolStripMenuItem.Enabled = false;
                }

                if (_profile.getLevelCount() + 1 >= 3)
                {
                    level03ToolStripMenuItem.Enabled = true;
                }
                else
                {
                    level03ToolStripMenuItem.Enabled = false;
                }

                if (_profile.getLevelCount() + 1 >= 4)
                {
                    level04ToolStripMenuItem.Enabled = true;
                }
                else
                {
                    level04ToolStripMenuItem.Enabled = false;
                }

                if (_profile.getLevelCount() + 1 >= 5)
                {
                    level05ToolStripMenuItem.Enabled = true;
                }
                else
                {
                    level05ToolStripMenuItem.Enabled = false;
                }

                if (_profile.getLevelCount() + 1 >= 6)
                {
                    level06ToolStripMenuItem.Enabled = true;
                }
                else
                {
                    level06ToolStripMenuItem.Enabled = false;
                }

                if (_profile.getLevelCount() + 1 >= 7)
                {
                    level07ToolStripMenuItem.Enabled = true;
                }
                else
                {
                    level07ToolStripMenuItem.Enabled = false;
                }

                if (_profile.getLevelCount() + 1 >= 8)
                {
                    level08ToolStripMenuItem.Enabled = true;
                }
                else
                {
                    level08ToolStripMenuItem.Enabled = false;
                }

                if (_profile.getLevelCount() + 1 >= 9)
                {
                    level09ToolStripMenuItem.Enabled = true;
                }
                else
                {
                    level09ToolStripMenuItem.Enabled = false;
                }

                if (_profile.getLevelCount() + 1 >= 10)
                {
                    level10ToolStripMenuItem.Enabled = true;
                }
                else
                {
                    level10ToolStripMenuItem.Enabled = false;
                }
            }
            else
            {
                level02ToolStripMenuItem.Enabled = false;
                level03ToolStripMenuItem.Enabled = false;
                level04ToolStripMenuItem.Enabled = false;
                level05ToolStripMenuItem.Enabled = false;
                level06ToolStripMenuItem.Enabled = false;
                level07ToolStripMenuItem.Enabled = false;
                level08ToolStripMenuItem.Enabled = false;
                level09ToolStripMenuItem.Enabled = false;
                level10ToolStripMenuItem.Enabled = false;
            }

            if (_profile.getLevelCount() + 1 >= 11)
            {
                level1120ToolStripMenuItem.Enabled = true;
                level11ToolStripMenuItem.Enabled = true;

                if (_profile.getLevelCount() + 1 >= 12)
                {
                    level12ToolStripMenuItem.Enabled = true;
                }
                else
                {
                    level12ToolStripMenuItem.Enabled = false;
                }

                if (_profile.getLevelCount() + 1 >= 13)
                {
                    level13ToolStripMenuItem.Enabled = true;
                }
                else
                {
                    level13ToolStripMenuItem.Enabled = false;
                }

                if (_profile.getLevelCount() + 1 >= 14)
                {
                    level14ToolStripMenuItem.Enabled = true;
                }
                else
                {
                    level14ToolStripMenuItem.Enabled = false;
                }

                if (_profile.getLevelCount() + 1 >= 15)
                {
                    level15ToolStripMenuItem.Enabled = true;
                }
                else
                {
                    level15ToolStripMenuItem.Enabled = false;
                }

                if (_profile.getLevelCount() + 1 >= 16)
                {
                    level16ToolStripMenuItem.Enabled = true;
                }
                else
                {
                    level16ToolStripMenuItem.Enabled = false;
                }

                if (_profile.getLevelCount() + 1 >= 17)
                {
                    level17ToolStripMenuItem.Enabled = true;
                }
                else
                {
                    level17ToolStripMenuItem.Enabled = false;
                }

                if (_profile.getLevelCount() + 1 >= 18)
                {
                    level18ToolStripMenuItem.Enabled = true;
                }
                else
                {
                    level18ToolStripMenuItem.Enabled = false;
                }

                if (_profile.getLevelCount() + 1 >= 19)
                {
                    level19ToolStripMenuItem.Enabled = true;
                }
                else
                {
                    level19ToolStripMenuItem.Enabled = false;
                }

                if (_profile.getLevelCount() + 1 >= 20)
                {
                    level20ToolStripMenuItem.Enabled = true;
                }
                else
                {
                    level20ToolStripMenuItem.Enabled = false;
                }
            }
            else
            {
                level1120ToolStripMenuItem.Enabled = false;
                level11ToolStripMenuItem.Enabled = false;
                level12ToolStripMenuItem.Enabled = false;
                level13ToolStripMenuItem.Enabled = false;
                level14ToolStripMenuItem.Enabled = false;
                level15ToolStripMenuItem.Enabled = false;
                level16ToolStripMenuItem.Enabled = false;
                level17ToolStripMenuItem.Enabled = false;
                level18ToolStripMenuItem.Enabled = false;
                level19ToolStripMenuItem.Enabled = false;
                level20ToolStripMenuItem.Enabled = false;
            }

            if (_profile.getLevelCount() + 1 >= 21)
            {
                level2130ToolStripMenuItem.Enabled = true;
                level21ToolStripMenuItem.Enabled = true;

                if (_profile.getLevelCount() + 1 >= 22)
                {
                    level22ToolStripMenuItem.Enabled = true;
                }
                else
                {
                    level22ToolStripMenuItem.Enabled = false;
                }

                if (_profile.getLevelCount() + 1 >= 23)
                {
                    level23ToolStripMenuItem.Enabled = true;
                }
                else
                {
                    level23ToolStripMenuItem.Enabled = false;
                }

                if (_profile.getLevelCount() + 1 >= 24)
                {
                    level24ToolStripMenuItem.Enabled = true;
                }
                else
                {
                    level24ToolStripMenuItem.Enabled = false;
                }

                if (_profile.getLevelCount() + 1 >= 25)
                {
                    level25ToolStripMenuItem.Enabled = true;
                }
                else
                {
                    level25ToolStripMenuItem.Enabled = false;
                }

                if (_profile.getLevelCount() + 1 >= 26)
                {
                    level26ToolStripMenuItem.Enabled = true;
                }
                else
                {
                    level26ToolStripMenuItem.Enabled = false;
                }

                if (_profile.getLevelCount() + 1 >= 27)
                {
                    level27ToolStripMenuItem.Enabled = true;
                }
                else
                {
                    level27ToolStripMenuItem.Enabled = false;
                }

                if (_profile.getLevelCount() + 1 >= 28)
                {
                    level28ToolStripMenuItem.Enabled = true;
                }
                else
                {
                    level28ToolStripMenuItem.Enabled = false;
                }

                if (_profile.getLevelCount() + 1 >= 29)
                {
                    level29ToolStripMenuItem.Enabled = true;
                }
                else
                {
                    level29ToolStripMenuItem.Enabled = false;
                }

                if (_profile.getLevelCount() + 1 >= 30)
                {
                    level30ToolStripMenuItem.Enabled = true;
                }
                else
                {
                    level30ToolStripMenuItem.Enabled = false;
                }
            }
            else
            {
                level2130ToolStripMenuItem.Enabled = false;
                level21ToolStripMenuItem.Enabled = false;
                level22ToolStripMenuItem.Enabled = false;
                level23ToolStripMenuItem.Enabled = false;
                level24ToolStripMenuItem.Enabled = false;
                level25ToolStripMenuItem.Enabled = false;
                level26ToolStripMenuItem.Enabled = false;
                level27ToolStripMenuItem.Enabled = false;
                level28ToolStripMenuItem.Enabled = false;
                level29ToolStripMenuItem.Enabled = false;
                level30ToolStripMenuItem.Enabled = false;
            }

            if (_profile.getLevelCount() + 1 >= 31)
            {
                level3140ToolStripMenuItem.Enabled = true;
                level31ToolStripMenuItem.Enabled = true;

                if (_profile.getLevelCount() + 1 >= 32)
                {
                    level32ToolStripMenuItem.Enabled = true;
                }
                else
                {
                    level32ToolStripMenuItem.Enabled = false;
                }

                if (_profile.getLevelCount() + 1 >= 33)
                {
                    level33ToolStripMenuItem.Enabled = true;
                }
                else
                {
                    level33ToolStripMenuItem.Enabled = false;
                }

                if (_profile.getLevelCount() + 1 >= 34)
                {
                    level34ToolStripMenuItem.Enabled = true;
                }
                else
                {
                    level34ToolStripMenuItem.Enabled = false;
                }

                if (_profile.getLevelCount() + 1 >= 35)
                {
                    level35ToolStripMenuItem.Enabled = true;
                }
                else
                {
                    level35ToolStripMenuItem.Enabled = false;
                }

                if (_profile.getLevelCount() + 1 >= 36)
                {
                    level36ToolStripMenuItem.Enabled = true;
                }
                else
                {
                    level36ToolStripMenuItem.Enabled = false;
                }

                if (_profile.getLevelCount() + 1 >= 37)
                {
                    level37ToolStripMenuItem.Enabled = true;
                }
                else
                {
                    level37ToolStripMenuItem.Enabled = false;
                }

                if (_profile.getLevelCount() + 1 >= 38)
                {
                    level38ToolStripMenuItem.Enabled = true;
                }
                else
                {
                    level38ToolStripMenuItem.Enabled = false;
                }

                if (_profile.getLevelCount() + 1 >= 39)
                {
                    level39ToolStripMenuItem.Enabled = true;
                }
                else
                {
                    level39ToolStripMenuItem.Enabled = false;
                }

                if (_profile.getLevelCount() + 1 >= 40)
                {
                    level40ToolStripMenuItem.Enabled = true;
                }
                else
                {
                    level40ToolStripMenuItem.Enabled = false;
                }
            }
            else
            {
                level3140ToolStripMenuItem.Enabled = false;
                level31ToolStripMenuItem.Enabled = false;
                level32ToolStripMenuItem.Enabled = false;
                level33ToolStripMenuItem.Enabled = false;
                level34ToolStripMenuItem.Enabled = false;
                level35ToolStripMenuItem.Enabled = false;
                level36ToolStripMenuItem.Enabled = false;
                level37ToolStripMenuItem.Enabled = false;
                level38ToolStripMenuItem.Enabled = false;
                level39ToolStripMenuItem.Enabled = false;
                level40ToolStripMenuItem.Enabled = false;
            }

            if (_profile.getLevelCount() + 1 >= 41)
            {
                level4150ToolStripMenuItem.Enabled = true;
                level41ToolStripMenuItem.Enabled = true;

                if (_profile.getLevelCount() + 1 >= 42)
                {
                    level42ToolStripMenuItem.Enabled = true;
                }
                else
                {
                    level42ToolStripMenuItem.Enabled = false;
                }

                if (_profile.getLevelCount() + 1 >= 43)
                {
                    level43ToolStripMenuItem.Enabled = true;
                }
                else
                {
                    level43ToolStripMenuItem.Enabled = false;
                }

                if (_profile.getLevelCount() + 1 >= 44)
                {
                    level44ToolStripMenuItem.Enabled = true;
                }
                else
                {
                    level44ToolStripMenuItem.Enabled = false;
                }

                if (_profile.getLevelCount() + 1 >= 45)
                {
                    level45ToolStripMenuItem.Enabled = true;
                }
                else
                {
                    level45ToolStripMenuItem.Enabled = false;
                }

                if (_profile.getLevelCount() + 1 >= 46)
                {
                    level46ToolStripMenuItem.Enabled = true;
                }
                else
                {
                    level46ToolStripMenuItem.Enabled = false;
                }

                if (_profile.getLevelCount() + 1 >= 47)
                {
                    level47ToolStripMenuItem.Enabled = true;
                }
                else
                {
                    level47ToolStripMenuItem.Enabled = false;
                }

                if (_profile.getLevelCount() + 1 >= 48)
                {
                    level48ToolStripMenuItem.Enabled = true;
                }
                else
                {
                    level48ToolStripMenuItem.Enabled = false;
                }

                if (_profile.getLevelCount() + 1 >= 49)
                {
                    level49ToolStripMenuItem.Enabled = true;
                }
                else
                {
                    level49ToolStripMenuItem.Enabled = false;
                }

                if (_profile.getLevelCount() + 1 >= 50)
                {
                    level50ToolStripMenuItem.Enabled = true;
                }
                else
                {
                    level50ToolStripMenuItem.Enabled = false;
                }
            }
            else
            {
                level4150ToolStripMenuItem.Enabled = false;
                level41ToolStripMenuItem.Enabled = false;
                level42ToolStripMenuItem.Enabled = false;
                level43ToolStripMenuItem.Enabled = false;
                level44ToolStripMenuItem.Enabled = false;
                level45ToolStripMenuItem.Enabled = false;
                level46ToolStripMenuItem.Enabled = false;
                level47ToolStripMenuItem.Enabled = false;
                level48ToolStripMenuItem.Enabled = false;
                level49ToolStripMenuItem.Enabled = false;
                level50ToolStripMenuItem.Enabled = false;
            }

        }

        private void level01ToolStripMenuItem_Click(object sender, EventArgs e)
        {
            _currentLevel = 1;
            changeLevelHelper();
        }

        private void level02ToolStripMenuItem_Click(object sender, EventArgs e)
        {
            _currentLevel = 2;
            changeLevelHelper();
        }

        private void level03ToolStripMenuItem_Click(object sender, EventArgs e)
        {
            _currentLevel = 3;
            changeLevelHelper();
        }

        private void level04ToolStripMenuItem_Click(object sender, EventArgs e)
        {
            _currentLevel = 4;
            changeLevelHelper();
        }

        private void level05ToolStripMenuItem_Click(object sender, EventArgs e)
        {
            _currentLevel = 5;
            changeLevelHelper();
        }

        private void level06ToolStripMenuItem_Click(object sender, EventArgs e)
        {
            _currentLevel = 6;
            changeLevelHelper();
        }

        private void level07ToolStripMenuItem_Click(object sender, EventArgs e)
        {
            _currentLevel = 7;
            changeLevelHelper();
        }

        private void level08ToolStripMenuItem_Click(object sender, EventArgs e)
        {
            _currentLevel = 8;
            changeLevelHelper();
        }

        private void level09ToolStripMenuItem_Click(object sender, EventArgs e)
        {
            _currentLevel = 9;
            changeLevelHelper();
        }

        private void level10ToolStripMenuItem_Click(object sender, EventArgs e)
        {
            _currentLevel = 10;
            changeLevelHelper();
        }

        private void level11ToolStripMenuItem_Click(object sender, EventArgs e)
        {
            _currentLevel = 11;
            changeLevelHelper();
        }

        private void level12ToolStripMenuItem_Click(object sender, EventArgs e)
        {
            _currentLevel = 12;
            changeLevelHelper();
        }

        private void level13ToolStripMenuItem_Click(object sender, EventArgs e)
        {
            _currentLevel = 13;
            changeLevelHelper();
        }

        private void level14ToolStripMenuItem_Click(object sender, EventArgs e)
        {
            _currentLevel = 14;
            changeLevelHelper();
        }

        private void level15ToolStripMenuItem_Click(object sender, EventArgs e)
        {
            _currentLevel = 15;
            changeLevelHelper();
        }

        private void level16ToolStripMenuItem_Click(object sender, EventArgs e)
        {
            _currentLevel = 16;
            changeLevelHelper();
        }

        private void level17ToolStripMenuItem_Click(object sender, EventArgs e)
        {
            _currentLevel = 17;
            changeLevelHelper();
        }

        private void level18ToolStripMenuItem_Click(object sender, EventArgs e)
        {
            _currentLevel = 18;
            changeLevelHelper();
        }

        private void level19ToolStripMenuItem_Click(object sender, EventArgs e)
        {
            _currentLevel = 19;
            changeLevelHelper();
        }

        private void level20ToolStripMenuItem_Click(object sender, EventArgs e)
        {
            _currentLevel = 20;
            changeLevelHelper();
        }

        private void level21ToolStripMenuItem_Click(object sender, EventArgs e)
        {
            _currentLevel = 21;
            changeLevelHelper();
        }

        private void level22ToolStripMenuItem_Click(object sender, EventArgs e)
        {
            _currentLevel = 22;
            changeLevelHelper();
        }

        private void level23ToolStripMenuItem_Click(object sender, EventArgs e)
        {
            _currentLevel = 23;
            changeLevelHelper();
        }

        private void level24ToolStripMenuItem_Click(object sender, EventArgs e)
        {
            _currentLevel = 24;
            changeLevelHelper();
        }

        private void level25ToolStripMenuItem_Click(object sender, EventArgs e)
        {
            _currentLevel = 25;
            changeLevelHelper();
        }

        private void level26ToolStripMenuItem_Click(object sender, EventArgs e)
        {
            _currentLevel = 26;
            changeLevelHelper();
        }

        private void level27ToolStripMenuItem_Click(object sender, EventArgs e)
        {
            _currentLevel = 27;
            changeLevelHelper();
        }

        private void level28ToolStripMenuItem_Click(object sender, EventArgs e)
        {
            _currentLevel = 28;
            changeLevelHelper();
        }

        private void level29ToolStripMenuItem_Click(object sender, EventArgs e)
        {
            _currentLevel = 29;
            changeLevelHelper();
        }

        private void level30ToolStripMenuItem_Click(object sender, EventArgs e)
        {
            _currentLevel = 30;
            changeLevelHelper();
        }

        private void level31ToolStripMenuItem_Click(object sender, EventArgs e)
        {
            _currentLevel = 31;
            changeLevelHelper();
        }

        private void level32ToolStripMenuItem_Click(object sender, EventArgs e)
        {
            _currentLevel = 32;
            changeLevelHelper();
        }

        private void level33ToolStripMenuItem_Click(object sender, EventArgs e)
        {
            _currentLevel = 33;
            changeLevelHelper();
        }

        private void level34ToolStripMenuItem_Click(object sender, EventArgs e)
        {
            _currentLevel = 34;
            changeLevelHelper();
        }

        private void level35ToolStripMenuItem_Click(object sender, EventArgs e)
        {
            _currentLevel = 35;
            changeLevelHelper();
        }

        private void level36ToolStripMenuItem_Click(object sender, EventArgs e)
        {
            _currentLevel = 36;
            changeLevelHelper();
        }

        private void level37ToolStripMenuItem_Click(object sender, EventArgs e)
        {
            _currentLevel = 37;
            changeLevelHelper();
        }

        private void level38ToolStripMenuItem_Click(object sender, EventArgs e)
        {
            _currentLevel = 38;
            changeLevelHelper();
        }

        private void level39ToolStripMenuItem_Click(object sender, EventArgs e)
        {
            _currentLevel = 39;
            changeLevelHelper();
        }

        private void level40ToolStripMenuItem_Click(object sender, EventArgs e)
        {
            _currentLevel = 40;
            changeLevelHelper();
        }

        private void level41ToolStripMenuItem_Click(object sender, EventArgs e)
        {
            _currentLevel = 41;
            changeLevelHelper();
        }

        private void level42ToolStripMenuItem_Click(object sender, EventArgs e)
        {
            _currentLevel = 42;
            changeLevelHelper();
        }

        private void level43ToolStripMenuItem_Click(object sender, EventArgs e)
        {
            _currentLevel = 43;
            changeLevelHelper();
        }

        private void level44ToolStripMenuItem_Click(object sender, EventArgs e)
        {
            _currentLevel = 44;
            changeLevelHelper();
        }

        private void level45ToolStripMenuItem_Click(object sender, EventArgs e)
        {
            _currentLevel = 45;
            changeLevelHelper();
        }

        private void level46ToolStripMenuItem_Click(object sender, EventArgs e)
        {
            _currentLevel = 46;
            changeLevelHelper();
        }

        private void level47ToolStripMenuItem_Click(object sender, EventArgs e)
        {
            _currentLevel = 47;
            changeLevelHelper();
        }

        private void level48ToolStripMenuItem_Click(object sender, EventArgs e)
        {
            _currentLevel = 48;
            changeLevelHelper();
        }

        private void level49ToolStripMenuItem_Click(object sender, EventArgs e)
        {
            _currentLevel = 49;
            changeLevelHelper();
        }

        private void level50ToolStripMenuItem_Click(object sender, EventArgs e)
        {
            _currentLevel = 50;
            changeLevelHelper();
        }
    }
}