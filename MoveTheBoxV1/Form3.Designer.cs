namespace MoveTheBoxV1
{
    partial class Form3
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(Form3));
            this.label_box = new System.Windows.Forms.Label();
            this.label_ground = new System.Windows.Forms.Label();
            this.label_goal = new System.Windows.Forms.Label();
            this.label_wall = new System.Windows.Forms.Label();
            this.panel_ground = new System.Windows.Forms.Panel();
            this.panel_box = new System.Windows.Forms.Panel();
            this.panel_goal = new System.Windows.Forms.Panel();
            this.panel_wall = new System.Windows.Forms.Panel();
            this.button1 = new System.Windows.Forms.Button();
            this.button2 = new System.Windows.Forms.Button();
            this.button3 = new System.Windows.Forms.Button();
            this.button4 = new System.Windows.Forms.Button();
            this.button5 = new System.Windows.Forms.Button();
            this.colorDialog1 = new System.Windows.Forms.ColorDialog();
            this.button_default = new System.Windows.Forms.Button();
            this.button_5 = new System.Windows.Forms.Button();
            this.panel_boxOnGoal = new System.Windows.Forms.Panel();
            this.label_boxOnGoal = new System.Windows.Forms.Label();
            this.SuspendLayout();
            // 
            // label_box
            // 
            this.label_box.AutoSize = true;
            this.label_box.BackColor = System.Drawing.Color.Transparent;
            this.label_box.Font = new System.Drawing.Font("Microsoft Sans Serif", 15F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label_box.Location = new System.Drawing.Point(8, 39);
            this.label_box.Name = "label_box";
            this.label_box.Size = new System.Drawing.Size(52, 25);
            this.label_box.TabIndex = 0;
            this.label_box.Text = "Box:";
            // 
            // label_ground
            // 
            this.label_ground.AutoSize = true;
            this.label_ground.BackColor = System.Drawing.Color.Transparent;
            this.label_ground.Font = new System.Drawing.Font("Microsoft Sans Serif", 15F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label_ground.Location = new System.Drawing.Point(8, 8);
            this.label_ground.Name = "label_ground";
            this.label_ground.Size = new System.Drawing.Size(83, 25);
            this.label_ground.TabIndex = 1;
            this.label_ground.Text = "Ground:";
            // 
            // label_goal
            // 
            this.label_goal.AutoSize = true;
            this.label_goal.BackColor = System.Drawing.Color.Transparent;
            this.label_goal.Font = new System.Drawing.Font("Microsoft Sans Serif", 15F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label_goal.Location = new System.Drawing.Point(8, 70);
            this.label_goal.Name = "label_goal";
            this.label_goal.Size = new System.Drawing.Size(100, 25);
            this.label_goal.TabIndex = 2;
            this.label_goal.Text = "Goal Field";
            // 
            // label_wall
            // 
            this.label_wall.AutoSize = true;
            this.label_wall.BackColor = System.Drawing.Color.Transparent;
            this.label_wall.Font = new System.Drawing.Font("Microsoft Sans Serif", 15F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label_wall.Location = new System.Drawing.Point(8, 101);
            this.label_wall.Name = "label_wall";
            this.label_wall.Size = new System.Drawing.Size(57, 25);
            this.label_wall.TabIndex = 3;
            this.label_wall.Text = "Wall:";
            // 
            // panel_ground
            // 
            this.panel_ground.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.panel_ground.Location = new System.Drawing.Point(154, 8);
            this.panel_ground.Name = "panel_ground";
            this.panel_ground.Size = new System.Drawing.Size(25, 25);
            this.panel_ground.TabIndex = 5;
            // 
            // panel_box
            // 
            this.panel_box.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.panel_box.Location = new System.Drawing.Point(154, 39);
            this.panel_box.Name = "panel_box";
            this.panel_box.Size = new System.Drawing.Size(25, 25);
            this.panel_box.TabIndex = 6;
            // 
            // panel_goal
            // 
            this.panel_goal.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.panel_goal.Location = new System.Drawing.Point(154, 70);
            this.panel_goal.Name = "panel_goal";
            this.panel_goal.Size = new System.Drawing.Size(25, 25);
            this.panel_goal.TabIndex = 7;
            // 
            // panel_wall
            // 
            this.panel_wall.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.panel_wall.Location = new System.Drawing.Point(154, 101);
            this.panel_wall.Name = "panel_wall";
            this.panel_wall.Size = new System.Drawing.Size(25, 25);
            this.panel_wall.TabIndex = 6;
            // 
            // button1
            // 
            this.button1.BackColor = System.Drawing.Color.Wheat;
            this.button1.Location = new System.Drawing.Point(204, 8);
            this.button1.Name = "button1";
            this.button1.Size = new System.Drawing.Size(63, 25);
            this.button1.TabIndex = 8;
            this.button1.Text = "change";
            this.button1.UseVisualStyleBackColor = false;
            this.button1.Click += new System.EventHandler(this.button1_Click);
            // 
            // button2
            // 
            this.button2.BackColor = System.Drawing.Color.Wheat;
            this.button2.Location = new System.Drawing.Point(204, 39);
            this.button2.Name = "button2";
            this.button2.Size = new System.Drawing.Size(63, 25);
            this.button2.TabIndex = 9;
            this.button2.Text = "change";
            this.button2.UseVisualStyleBackColor = false;
            this.button2.Click += new System.EventHandler(this.button2_Click);
            // 
            // button3
            // 
            this.button3.BackColor = System.Drawing.Color.Wheat;
            this.button3.Location = new System.Drawing.Point(204, 70);
            this.button3.Name = "button3";
            this.button3.Size = new System.Drawing.Size(63, 25);
            this.button3.TabIndex = 10;
            this.button3.Text = "change";
            this.button3.UseVisualStyleBackColor = false;
            this.button3.Click += new System.EventHandler(this.button3_Click);
            // 
            // button4
            // 
            this.button4.BackColor = System.Drawing.Color.Wheat;
            this.button4.Location = new System.Drawing.Point(204, 101);
            this.button4.Name = "button4";
            this.button4.Size = new System.Drawing.Size(63, 25);
            this.button4.TabIndex = 11;
            this.button4.Text = "change";
            this.button4.UseVisualStyleBackColor = false;
            this.button4.Click += new System.EventHandler(this.button4_Click);
            // 
            // button5
            // 
            this.button5.BackColor = System.Drawing.Color.Wheat;
            this.button5.Location = new System.Drawing.Point(184, 184);
            this.button5.Name = "button5";
            this.button5.Size = new System.Drawing.Size(83, 31);
            this.button5.TabIndex = 13;
            this.button5.Text = "OK";
            this.button5.UseVisualStyleBackColor = false;
            this.button5.Click += new System.EventHandler(this.button5_Click);
            // 
            // button_default
            // 
            this.button_default.BackColor = System.Drawing.Color.Wheat;
            this.button_default.Location = new System.Drawing.Point(65, 163);
            this.button_default.Name = "button_default";
            this.button_default.Size = new System.Drawing.Size(113, 25);
            this.button_default.TabIndex = 17;
            this.button_default.Text = "default colors";
            this.button_default.UseVisualStyleBackColor = false;
            this.button_default.Click += new System.EventHandler(this.button_default_Click);
            // 
            // button_5
            // 
            this.button_5.BackColor = System.Drawing.Color.Wheat;
            this.button_5.Location = new System.Drawing.Point(204, 132);
            this.button_5.Name = "button_5";
            this.button_5.Size = new System.Drawing.Size(63, 25);
            this.button_5.TabIndex = 20;
            this.button_5.Text = "change";
            this.button_5.UseVisualStyleBackColor = false;
            this.button_5.Click += new System.EventHandler(this.button_5_Click);
            // 
            // panel_boxOnGoal
            // 
            this.panel_boxOnGoal.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.panel_boxOnGoal.Location = new System.Drawing.Point(154, 132);
            this.panel_boxOnGoal.Name = "panel_boxOnGoal";
            this.panel_boxOnGoal.Size = new System.Drawing.Size(25, 25);
            this.panel_boxOnGoal.TabIndex = 19;
            // 
            // label_boxOnGoal
            // 
            this.label_boxOnGoal.AutoSize = true;
            this.label_boxOnGoal.BackColor = System.Drawing.Color.Transparent;
            this.label_boxOnGoal.Font = new System.Drawing.Font("Microsoft Sans Serif", 15F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label_boxOnGoal.Location = new System.Drawing.Point(8, 132);
            this.label_boxOnGoal.Name = "label_boxOnGoal";
            this.label_boxOnGoal.Size = new System.Drawing.Size(130, 25);
            this.label_boxOnGoal.TabIndex = 18;
            this.label_boxOnGoal.Text = "Box On Goal:";
            // 
            // Form3
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.Wheat;
            this.ClientSize = new System.Drawing.Size(283, 226);
            this.Controls.Add(this.button_5);
            this.Controls.Add(this.panel_boxOnGoal);
            this.Controls.Add(this.label_boxOnGoal);
            this.Controls.Add(this.button_default);
            this.Controls.Add(this.button5);
            this.Controls.Add(this.button4);
            this.Controls.Add(this.button3);
            this.Controls.Add(this.button2);
            this.Controls.Add(this.button1);
            this.Controls.Add(this.panel_wall);
            this.Controls.Add(this.panel_goal);
            this.Controls.Add(this.panel_box);
            this.Controls.Add(this.panel_ground);
            this.Controls.Add(this.label_wall);
            this.Controls.Add(this.label_goal);
            this.Controls.Add(this.label_ground);
            this.Controls.Add(this.label_box);
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.MaximizeBox = false;
            this.MaximumSize = new System.Drawing.Size(299, 262);
            this.MinimumSize = new System.Drawing.Size(299, 262);
            this.Name = "Form3";
            this.SizeGripStyle = System.Windows.Forms.SizeGripStyle.Hide;
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Colors";
            this.Load += new System.EventHandler(this.Form3_Load);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label label_box;
        private System.Windows.Forms.Label label_ground;
        private System.Windows.Forms.Label label_goal;
        private System.Windows.Forms.Label label_wall;
        private System.Windows.Forms.Panel panel_ground;
        private System.Windows.Forms.Panel panel_box;
        private System.Windows.Forms.Panel panel_goal;
        private System.Windows.Forms.Panel panel_wall;
        private System.Windows.Forms.Button button1;
        private System.Windows.Forms.Button button2;
        private System.Windows.Forms.Button button3;
        private System.Windows.Forms.Button button4;
        private System.Windows.Forms.Button button5;
        private System.Windows.Forms.ColorDialog colorDialog1;
        private System.Windows.Forms.Button button_default;
        private System.Windows.Forms.Button button_5;
        private System.Windows.Forms.Panel panel_boxOnGoal;
        private System.Windows.Forms.Label label_boxOnGoal;
    }
}