using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;

namespace MoveTheBoxV1
{
    public partial class Form3 : Form
    {
        public Form2 form2;
        private Color _oldBox;
        private Color _oldGoal;
        private Color _oldGround;
        private Color _oldWall;
        private Color _oldBoxOnGoal;

        public Form3()
        {
            InitializeComponent();
        }

        private void Form3_Load(object sender, EventArgs e)
        {
            panel_goal.BackColor = form2._goalColor;
            panel_wall.BackColor = form2._wallColor;
            panel_box.BackColor = form2._boxColor;
            panel_ground.BackColor = form2._groundColor;
            panel_boxOnGoal.BackColor = form2._boxOnGoalColor;

            _oldBox = form2._boxColor;
            _oldGoal = form2._goalColor;
            _oldGround = form2._groundColor;
            _oldWall = form2._wallColor;
            _oldBoxOnGoal = form2._boxOnGoalColor;
        }

        private void button5_Click(object sender, EventArgs e)
        {
            form2._groundColor = panel_ground.BackColor;
            form2._boxColor = panel_box.BackColor;
            form2._goalColor = panel_goal.BackColor;
            form2._wallColor = panel_wall.BackColor;
            form2._boxOnGoalColor = panel_boxOnGoal.BackColor;

            refreshColors();
            this.Close();
        }

        private void button1_Click(object sender, EventArgs e)
        {
            colorDialog1.ShowDialog();
            panel_ground.BackColor = colorDialog1.Color;
        }

        private void button2_Click(object sender, EventArgs e)
        {
            colorDialog1.ShowDialog();
            panel_box.BackColor = colorDialog1.Color;         
        }

        private void button3_Click(object sender, EventArgs e)
        {
            colorDialog1.ShowDialog();
            panel_goal.BackColor = colorDialog1.Color;    
        }

        private void button4_Click(object sender, EventArgs e)
        {
            colorDialog1.ShowDialog();
            panel_wall.BackColor = colorDialog1.Color;    
        }

        private void button_5_Click(object sender, EventArgs e)
        {
            colorDialog1.ShowDialog();
            panel_boxOnGoal.BackColor = colorDialog1.Color; 
        }

        private void refreshColors()
        {
            Color color;

            for (int row = 0; row < 25; row++)
            {
                for (int col = 0; col < 25; col++)
                {
                    color = form2._panelMatrix[row, col].BackColor;
                    if (color == _oldGround)
                    {
                        form2._panelMatrix[row, col].BackColor = panel_ground.BackColor;
                    }
                    else if (color == _oldGoal)
                    {
                        form2._panelMatrix[row, col].BackColor = panel_goal.BackColor;
                    }
                    else if (color == _oldBox)
                    {
                        form2._panelMatrix[row, col].BackColor = panel_box.BackColor;
                    }
                    else if (color == _oldWall)
                    {
                        form2._panelMatrix[row, col].BackColor = panel_wall.BackColor;
                    }
                    else if (color == _oldBoxOnGoal)
                    {
                        form2._panelMatrix[row, col].BackColor = panel_boxOnGoal.BackColor;
                    }
                }
            }
        }

        private void button_default_Click(object sender, EventArgs e)
        {
            panel_ground.BackColor = form2._defaultColors[0];
            panel_wall.BackColor = form2._defaultColors[1];
            panel_box.BackColor = form2._defaultColors[2];
            panel_goal.BackColor = form2._defaultColors[3];
            panel_boxOnGoal.BackColor = form2._defaultColors[4];
        }
    }
}