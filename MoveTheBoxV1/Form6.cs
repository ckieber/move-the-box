using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;

namespace MoveTheBoxV1
{
    public partial class Form6 : Form
    {
        public Form2 form2;

        public Form6()
        {
            InitializeComponent();
        }

        private void button_ok_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void Form6_Load(object sender, EventArgs e)
        {
            label_time.Text = "Time: " + formatString(form2._timeDifference);
            label_moves.Text = "Moves: " + form2._moves;
            label_score.Text = "Score: " + calculateScore(form2._timeDifference, form2._moves).ToString().PadLeft(4, '0');
        }

        private string formatString(TimeSpan time)
        {
            string minutes = "", seconds = "";

            if (time.Minutes.ToString().Length == 1)
            {
                minutes = "0" + time.Minutes.ToString();
            }
            else
            {
                minutes = time.Minutes.ToString();
            }
            minutes += "m";

            if (time.Seconds.ToString().Length == 1)
            {
                seconds = "0" + time.Seconds.ToString();
            }
            else
            {
                seconds = time.Seconds.ToString();
            }
            seconds += "s";

            return minutes + seconds;
        }

        private int calculateScore(TimeSpan gameTime, int moves)
        {
            int seconds = gameTime.Minutes * 60;
            seconds += gameTime.Seconds;

            int secondPoints = seconds * Record._timeMultiplier;
            int movePoints = moves * Record._moveMultiplier;

            int score = secondPoints + movePoints;

            score = Record._maxScore - score;
            if (score < 0)
            {
                return 0;
            }
            return score;
        }
    }
}