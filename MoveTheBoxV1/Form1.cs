using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;
using System.IO;
using System.Runtime.Serialization;
using System.Runtime.Serialization.Formatters;
using System.Runtime.Serialization.Formatters.Binary;

namespace MoveTheBoxV1
{
    public partial class Form1 : Form
    {
        public string _playerName;
        private bool _loadedSuccessfully;
        private Form2 form2;
        public Point _centerScreenLocation;

        public Form1()
        {
            InitializeComponent();
            button_start.Enabled = false;
            _loadedSuccessfully = false;
        }

        private void Form1_Shown(object sender, EventArgs e)
        {
            _centerScreenLocation = new Point(Location.X, Location.Y);
        }

        private void button_start_Click(object sender, EventArgs e)
        {
            form2 = new Form2();
            form2.form1 = this;

            _playerName = changeName(textBox_name.Text);

            this.Hide();
            form2.Show();
        }

        private void textBox_name_TextChanged(object sender, EventArgs e)
        {
            if (textBox_name.Text == "" || textBox_name.Text.Length >= 10)
                button_start.Enabled = false;
            else
                button_start.Enabled = true;
        }

        private string changeName(string name)
        {
            string firstLetter = name.Substring(0, 1).ToUpper();
            string rest = name.Substring(1).ToLower();

            return firstLetter + rest;
        }

        private void exitToolStripMenuItem_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void Form1_VisibleChanged(object sender, EventArgs e)
        {
            textBox_name.Text = "";
        }

        private void loadToolStripMenuItem_Click(object sender, EventArgs e)
        {
            //settings for the open box
            openFileDialog1.Filter = "Move The Box File (.mtb)|*.mtb";
            openFileDialog1.CheckPathExists = true;
            openFileDialog1.AddExtension = true;
            openFileDialog1.DefaultExt = "mtb";
            openFileDialog1.RestoreDirectory = false;
            openFileDialog1.ValidateNames = true;
            openFileDialog1.ShowDialog();

            //after window is closed show infobox
            if (_loadedSuccessfully)
            {
                form2._currentLevel--;
                form2._infoBox = false;
                form2.changeLevel();
                form2._infoBox = true;
                _loadedSuccessfully = false;

                this.Hide();
                form2.Show();
                form2.Focus();
            }
        }

        private void openFileDialog1_FileOk(object sender, CancelEventArgs e)
        {
            string fileName = openFileDialog1.FileName;
            form2 = new Form2();
            form2.form1 = this;

            if (fileName != "")
            {
                try
                {
                    Stream stream = File.Open(fileName, FileMode.Open);
                    BinaryFormatter formatter = new BinaryFormatter();
                    FileInformation info = (FileInformation)formatter.Deserialize(stream);
                    stream.Close();

                    _playerName = info._playerName;
                    form2._currentLevel = info._level;
                    //order: ground, wall, box, goal
                    form2._groundColor = Color.FromArgb(info._colors[0]);
                    form2._wallColor = Color.FromArgb(info._colors[1]);
                    form2._boxColor = Color.FromArgb(info._colors[2]);
                    form2._goalColor = Color.FromArgb(info._colors[3]);
                    form2._boxOnGoalColor = Color.FromArgb(info._colors[4]);

                    _loadedSuccessfully = true;                   
                }
                catch
                {
                    MessageBox.Show("Error occured while loading the file!", "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
                }
            }
        }

        private void rulesToolStripMenuItem_Click(object sender, EventArgs e)
        {
            Form9 rules = new Form9();
            rules.ShowDialog();
        }

        private void aboutToolStripMenuItem1_Click(object sender, EventArgs e)
        {
            Form11 about = new Form11();
            about.ShowDialog();
        }
    }
}