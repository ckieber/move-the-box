using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;

namespace MoveTheBoxV1
{
    public partial class Form4 : Form
    {
        public Form2 form2;
        private string _break = "----------------------------\r\n";
        public bool _clearResult;

        public Form4()
        {
            InitializeComponent();
            _clearResult = false;
        }

        private void Form4_Load(object sender, EventArgs e)
        {
            label_name.Text = "Name: " + form2._playerName;
            loadTimes();
        }

        private void button_ok_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void loadTimes()
        {
            if (form2._profile.getLevelCount() == 0)
            {
                richTextBox_times.Text = "Nothing available";
            }
            else
            {
                richTextBox_times.Text = _break;
                for (int i = 0; i < form2._profile.getLevelCount(); i++)
                {
                    richTextBox_times.Text += "Level " + (i + 1).ToString().PadLeft(2, '0') + ": \r\n" +
                        "Score: " + form2._profile.getScore(i + 1).ToString().PadLeft(4, '0') + "\r\n" +  
                        "Moves: " + form2._profile.getMoves(i + 1) + "\r\n" +
                        "Time: " + formatString(form2._profile.getTime(i + 1)) + "\r\n";
                    richTextBox_times.Text += _break;
                    richTextBox_times.SelectionStart = richTextBox_times.Text.Length;
                    richTextBox_times.ScrollToCaret();
                }
            }
        }

        private string formatString(TimeSpan time)
        {
            string minutes = "", seconds = "";

            if (time.Minutes.ToString().Length == 1)
            {
                minutes = "0" + time.Minutes.ToString();
            }
            else
            {
                minutes = time.Minutes.ToString();
            }
            minutes += "m";

            if (time.Seconds.ToString().Length == 1)
            {
                seconds = "0" + time.Seconds.ToString();
            }
            else
            {
                seconds = time.Seconds.ToString();
            }
            seconds += "s";

            return minutes + seconds;
        }

        private void button_reset_Click(object sender, EventArgs e)
        {
            Form7 reset = new Form7();
            reset.form4 = this;
            reset.ShowDialog();

            if (_clearResult)
            {
                form2._profile.clear();
                richTextBox_times.Text = "No times available";          
            }
        }

    }
}