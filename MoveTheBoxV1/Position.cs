using System;
using System.Collections.Generic;
using System.Text;

namespace MoveTheBoxV1
{
    class Position
    {
        private int _row;
        private int _col;

        public Position()
        {
            _row = 0;
            _col = 0;
        }

        public Position(int row, int col)
        {
            _row = row;
            _col = col;
        }

        public int getRow()
        {
            return _row;
        }

        public void setRow(int row)
        {
            _row = row;
        }

        public int getCol()
        {
            return _col;
        }

        public void setCol(int col)
        {
            _col = col;
        }

        public override bool Equals(object obj)
        {
            bool equals = false;

            if (_row == ((Position)obj).getRow() && _col == ((Position)obj).getCol())
            {
                equals = true;
            }

            return equals;
        }
    }
}
