using System;
using System.Collections.Generic;
using System.Text;
using System.Runtime.Serialization;
using System.Runtime.Serialization.Formatters.Binary;

namespace MoveTheBoxV1
{
    [Serializable]
    class FileInformation : ISerializable
    {
        public string _playerName;
        public int[] _colors;
        public int _level;

        public FileInformation()
        {
            _colors = new int[5];
            _level = 1;
            _playerName = "";
        }

        //Serialization function.
        public void GetObjectData(SerializationInfo info, StreamingContext ctxt)
        {
            info.AddValue("playerName", _playerName);
            info.AddValue("level", _level);
            info.AddValue("colors", _colors);
        }

        public FileInformation(SerializationInfo info, StreamingContext ctxt)
        {
            _playerName = (string)info.GetValue("playerName", typeof(string));
            _level = (int)info.GetValue("level", typeof(int));            
            _colors = (int[])info.GetValue("colors", typeof(int[]));
        }
    }
}
