namespace MoveTheBoxV1
{
    partial class Form5
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(Form5));
            this.label_level = new System.Windows.Forms.Label();
            this.label_time = new System.Windows.Forms.Label();
            this.button_start = new System.Windows.Forms.Button();
            this.label_moves = new System.Windows.Forms.Label();
            this.label_score = new System.Windows.Forms.Label();
            this.SuspendLayout();
            // 
            // label_level
            // 
            this.label_level.AutoSize = true;
            this.label_level.BackColor = System.Drawing.Color.Transparent;
            this.label_level.Location = new System.Drawing.Point(8, 8);
            this.label_level.Name = "label_level";
            this.label_level.Size = new System.Drawing.Size(33, 13);
            this.label_level.TabIndex = 0;
            this.label_level.Text = "Level";
            // 
            // label_time
            // 
            this.label_time.AutoSize = true;
            this.label_time.BackColor = System.Drawing.Color.Transparent;
            this.label_time.Location = new System.Drawing.Point(8, 47);
            this.label_time.Name = "label_time";
            this.label_time.Size = new System.Drawing.Size(33, 13);
            this.label_time.TabIndex = 1;
            this.label_time.Text = "Time:";
            // 
            // button_start
            // 
            this.button_start.BackColor = System.Drawing.Color.Wheat;
            this.button_start.Location = new System.Drawing.Point(12, 72);
            this.button_start.Name = "button_start";
            this.button_start.Size = new System.Drawing.Size(83, 31);
            this.button_start.TabIndex = 15;
            this.button_start.Text = "Start";
            this.button_start.UseVisualStyleBackColor = false;
            this.button_start.Click += new System.EventHandler(this.button_start_Click);
            // 
            // label_moves
            // 
            this.label_moves.AutoSize = true;
            this.label_moves.BackColor = System.Drawing.Color.Transparent;
            this.label_moves.Location = new System.Drawing.Point(8, 34);
            this.label_moves.Name = "label_moves";
            this.label_moves.Size = new System.Drawing.Size(42, 13);
            this.label_moves.TabIndex = 16;
            this.label_moves.Text = "Moves:";
            // 
            // label_score
            // 
            this.label_score.AutoSize = true;
            this.label_score.BackColor = System.Drawing.Color.Transparent;
            this.label_score.Location = new System.Drawing.Point(8, 21);
            this.label_score.Name = "label_score";
            this.label_score.Size = new System.Drawing.Size(38, 13);
            this.label_score.TabIndex = 17;
            this.label_score.Text = "Score:";
            // 
            // Form5
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.Wheat;
            this.ClientSize = new System.Drawing.Size(108, 115);
            this.ControlBox = false;
            this.Controls.Add(this.label_score);
            this.Controls.Add(this.label_moves);
            this.Controls.Add(this.button_start);
            this.Controls.Add(this.label_time);
            this.Controls.Add(this.label_level);
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.MaximizeBox = false;
            this.MaximumSize = new System.Drawing.Size(124, 131);
            this.MinimumSize = new System.Drawing.Size(124, 131);
            this.Name = "Form5";
            this.SizeGripStyle = System.Windows.Forms.SizeGripStyle.Hide;
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Load += new System.EventHandler(this.Form5_Load);
            this.FormClosed += new System.Windows.Forms.FormClosedEventHandler(this.Form5_FormClosed);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label label_level;
        private System.Windows.Forms.Label label_time;
        private System.Windows.Forms.Button button_start;
        private System.Windows.Forms.Label label_moves;
        private System.Windows.Forms.Label label_score;
    }
}