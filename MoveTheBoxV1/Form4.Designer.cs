namespace MoveTheBoxV1
{
    partial class Form4
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(Form4));
            this.label_name = new System.Windows.Forms.Label();
            this.richTextBox_times = new System.Windows.Forms.RichTextBox();
            this.button_ok = new System.Windows.Forms.Button();
            this.button_reset = new System.Windows.Forms.Button();
            this.SuspendLayout();
            // 
            // label_name
            // 
            this.label_name.AutoSize = true;
            this.label_name.BackColor = System.Drawing.Color.Transparent;
            this.label_name.Location = new System.Drawing.Point(8, 8);
            this.label_name.Name = "label_name";
            this.label_name.Size = new System.Drawing.Size(38, 13);
            this.label_name.TabIndex = 0;
            this.label_name.Text = "Name:";
            // 
            // richTextBox_times
            // 
            this.richTextBox_times.BackColor = System.Drawing.Color.Wheat;
            this.richTextBox_times.Location = new System.Drawing.Point(8, 32);
            this.richTextBox_times.Name = "richTextBox_times";
            this.richTextBox_times.ReadOnly = true;
            this.richTextBox_times.ScrollBars = System.Windows.Forms.RichTextBoxScrollBars.ForcedVertical;
            this.richTextBox_times.Size = new System.Drawing.Size(112, 184);
            this.richTextBox_times.TabIndex = 1;
            this.richTextBox_times.Text = "";
            // 
            // button_ok
            // 
            this.button_ok.BackColor = System.Drawing.Color.Wheat;
            this.button_ok.Location = new System.Drawing.Point(143, 185);
            this.button_ok.Name = "button_ok";
            this.button_ok.Size = new System.Drawing.Size(83, 31);
            this.button_ok.TabIndex = 14;
            this.button_ok.Text = "OK";
            this.button_ok.UseVisualStyleBackColor = false;
            this.button_ok.Click += new System.EventHandler(this.button_ok_Click);
            // 
            // button_reset
            // 
            this.button_reset.BackColor = System.Drawing.Color.Wheat;
            this.button_reset.Location = new System.Drawing.Point(143, 148);
            this.button_reset.Name = "button_reset";
            this.button_reset.Size = new System.Drawing.Size(83, 31);
            this.button_reset.TabIndex = 15;
            this.button_reset.Text = "Reset";
            this.button_reset.UseVisualStyleBackColor = false;
            this.button_reset.Click += new System.EventHandler(this.button_reset_Click);
            // 
            // Form4
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.Wheat;
            this.ClientSize = new System.Drawing.Size(246, 235);
            this.Controls.Add(this.button_reset);
            this.Controls.Add(this.button_ok);
            this.Controls.Add(this.richTextBox_times);
            this.Controls.Add(this.label_name);
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.MaximizeBox = false;
            this.MaximumSize = new System.Drawing.Size(254, 262);
            this.MinimumSize = new System.Drawing.Size(254, 262);
            this.Name = "Form4";
            this.SizeGripStyle = System.Windows.Forms.SizeGripStyle.Hide;
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Record";
            this.Load += new System.EventHandler(this.Form4_Load);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label label_name;
        private System.Windows.Forms.RichTextBox richTextBox_times;
        private System.Windows.Forms.Button button_ok;
        private System.Windows.Forms.Button button_reset;
    }
}