using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;

namespace MoveTheBoxV1
{
    public partial class Form5 : Form
    {
        public Form2 form2;

        public Form5()
        {
            InitializeComponent();
        }

        private void Form5_Load(object sender, EventArgs e)
        {
            label_level.Text = "Level " + form2._currentLevel.ToString().PadLeft(2, '0');
            label_score.Text = "Score: " + form2._profile.getScore(form2._currentLevel).ToString().PadLeft(4, '0');
            label_moves.Text = "Moves: " + form2._profile.getMoves(form2._currentLevel);
            label_time.Text = "Time: " + formatString(form2._profile.getTime(form2._currentLevel));    
        }

        private void button_start_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private string formatString(TimeSpan time)
        {
            string minutes = "", seconds = "";

            if (time.Minutes.ToString().Length == 1)
            {
                minutes = "0" + time.Minutes.ToString();
            }
            else
            {
                minutes = time.Minutes.ToString();
            }
            minutes += "m";

            if (time.Seconds.ToString().Length == 1)
            {
                seconds = "0" + time.Seconds.ToString();
            }
            else
            {
                seconds = time.Seconds.ToString();
            }
            seconds += "s";

            return minutes + seconds;
        }

        private void Form5_FormClosed(object sender, FormClosedEventArgs e)
        {
            form2._startTime = DateTime.Now;
        }
    }
}