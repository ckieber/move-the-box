namespace MoveTheBoxV1
{
    partial class Form6
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(Form6));
            this.label_time = new System.Windows.Forms.Label();
            this.button_ok = new System.Windows.Forms.Button();
            this.label_moves = new System.Windows.Forms.Label();
            this.label_score = new System.Windows.Forms.Label();
            this.SuspendLayout();
            // 
            // label_time
            // 
            this.label_time.AutoSize = true;
            this.label_time.BackColor = System.Drawing.Color.Transparent;
            this.label_time.Location = new System.Drawing.Point(16, 34);
            this.label_time.Name = "label_time";
            this.label_time.Size = new System.Drawing.Size(36, 13);
            this.label_time.TabIndex = 0;
            this.label_time.Text = "Time: ";
            // 
            // button_ok
            // 
            this.button_ok.BackColor = System.Drawing.Color.Wheat;
            this.button_ok.Location = new System.Drawing.Point(12, 59);
            this.button_ok.Name = "button_ok";
            this.button_ok.Size = new System.Drawing.Size(83, 31);
            this.button_ok.TabIndex = 16;
            this.button_ok.Text = "OK";
            this.button_ok.UseVisualStyleBackColor = false;
            this.button_ok.Click += new System.EventHandler(this.button_ok_Click);
            // 
            // label_moves
            // 
            this.label_moves.AutoSize = true;
            this.label_moves.BackColor = System.Drawing.Color.Transparent;
            this.label_moves.Location = new System.Drawing.Point(16, 21);
            this.label_moves.Name = "label_moves";
            this.label_moves.Size = new System.Drawing.Size(45, 13);
            this.label_moves.TabIndex = 17;
            this.label_moves.Text = "Moves: ";
            // 
            // label_score
            // 
            this.label_score.AutoSize = true;
            this.label_score.BackColor = System.Drawing.Color.Transparent;
            this.label_score.Location = new System.Drawing.Point(16, 9);
            this.label_score.Name = "label_score";
            this.label_score.Size = new System.Drawing.Size(41, 13);
            this.label_score.TabIndex = 18;
            this.label_score.Text = "Score: ";
            // 
            // Form6
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.Wheat;
            this.ClientSize = new System.Drawing.Size(107, 103);
            this.ControlBox = false;
            this.Controls.Add(this.label_score);
            this.Controls.Add(this.label_moves);
            this.Controls.Add(this.button_ok);
            this.Controls.Add(this.label_time);
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.MaximizeBox = false;
            this.MaximumSize = new System.Drawing.Size(115, 111);
            this.MinimumSize = new System.Drawing.Size(115, 111);
            this.Name = "Form6";
            this.SizeGripStyle = System.Windows.Forms.SizeGripStyle.Hide;
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Load += new System.EventHandler(this.Form6_Load);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label label_time;
        private System.Windows.Forms.Button button_ok;
        private System.Windows.Forms.Label label_moves;
        private System.Windows.Forms.Label label_score;
    }
}