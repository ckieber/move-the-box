using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;

namespace MoveTheBoxV1
{
    public partial class Form7 : Form
    {
        public Form4 form4;

        public Form7()
        {
            InitializeComponent();
        }

        private void button_yes_Click(object sender, EventArgs e)
        {
            form4._clearResult = true;
            this.Close();
        }

        private void button_no_Click(object sender, EventArgs e)
        {
            form4._clearResult = false;
            this.Close();
        }
    }
}