using System;
using System.Collections.Generic;
using System.Text;
using System.Runtime.Serialization;
using System.Runtime.Serialization.Formatters.Binary;

namespace MoveTheBoxV1
{
    [Serializable]
    public class Record
    {
        private string _name;
        private List<TimeSpan> _times;
        private List<int> _moves;
        private List<int> _score;
        public static int _maxScore = 10000;
        public static int _moveMultiplier = 1;
        public static int _timeMultiplier = 6;

        public Record()
        {
            _name = "";
            _times = new List<TimeSpan>();
            _moves = new List<int>();
            _score = new List<int>();
        }

        public Record(string name)
        {
            _name = name;
            _times = new List<TimeSpan>();
            _moves = new List<int>();
            _score = new List<int>();
        }

        public string getName()
        {
            return _name;
        }

        public override bool Equals(object obj)
        {
            return _name.Equals(((Record)obj).getName());
        }

        public override int GetHashCode()
        {
            return _name.GetHashCode();
        }

        public void clear()
        {
            _times = new List<TimeSpan>();
            _moves = new List<int>();
            _score = new List<int>();
        }

        public int calculateScore(TimeSpan gameTime, int moves)
        {
            int seconds = gameTime.Minutes * 60;
            seconds += gameTime.Seconds;

            int secondPoints = seconds * _timeMultiplier;
            int movePoints = moves * _moveMultiplier;

            int score = secondPoints + movePoints;

            score = _maxScore - score;
            if (score < 0)
            {
                return 0;
            }
            return score;
        }

        public void update(int level, TimeSpan gameTime, int moves)
        {
            int score = calculateScore(gameTime, moves);
            if (level > _score.Count)
            {
                int i = 0;
                while (i < _score.Count)
                {
                    i++;
                }

                while (i < level-1)
                {
                    _score.Add(0);
                    _times.Add(new TimeSpan(0, 59, 59));
                    _moves.Add(0);
                    i++;
                }
                _times.Add(gameTime);
                _moves.Add(moves);
                _score.Add(score);
            }
            else
            {
                if (score > _score[level - 1])
                {
                    _moves[level - 1] = moves;
                    _times[level - 1] = gameTime;
                    _score[level - 1] = score;
                }
                else if (score == _score[level - 1])
                {
                    if (moves < _moves[level - 1])
                    {
                        _moves[level - 1] = moves;
                        _times[level - 1] = gameTime;
                    }
                    else if (moves == _moves[level - 1])
                    {
                        if (gameTime < _times[level - 1])
                        {
                            _times[level - 1] = gameTime;
                        }
                    }
                }
            }
        }

        public TimeSpan getTime(int level)
        {
            if (level > _times.Count)
            {
                return new TimeSpan(0,59,59);
            }
            else
            {
                return _times[level - 1];
            }
        }

        public int getMoves(int level)
        {
            if (level > _moves.Count)
            {
                return 0;
            }
            else
            {
                return _moves[level - 1];
            }
        }

        public int getScore(int level)
        {
            if (level > _score.Count)
            {
                return 0;
            }
            else
            {
                return _score[level - 1];
            }
        }

        public int getLevelCount()
        {
            return _times.Count;
        }

        //Serialization function.
        public void GetObjectData(SerializationInfo info, StreamingContext ctxt)
        {
            info.AddValue("name", _name);
            info.AddValue("times", _times);
        }

        public Record(SerializationInfo info, StreamingContext ctxt)
        {
            _name = (string)info.GetValue("name", typeof(string));
            _times = (List<TimeSpan>)info.GetValue("times", typeof(List<TimeSpan>));
        }
    }
}
